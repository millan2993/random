/**
 * Created by alex on 28/10/17.
 */

var urlShort = 'a.url/';

document.getElementById('get').addEventListener('click', function () {
    var unique = ((+new Date)+Date.now()).toString(32);
    var short = document.getElementById('short');
    var url = document.getElementById('url');

    if (url.value == '') {
        short.textContent = unique;
        short.removeAttribute('href');
        // short.classList.remove('tag');
        // short.classList.add('url');
    } else {
        short.textContent = urlShort + unique;
        short.setAttribute('href', 'http://' + urlShort + unique);
        // short.classList.remove('url');
        // short.classList.add('tag')
    }

});

