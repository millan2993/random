var gulp    = require('gulp');
var minifyHTML = require("gulp-minify-html");
var minifyCSS  = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var uncss   = require('gulp-uncss');
var imageOP = require('gulp-image-optimization');
var changeImg = require('gulp-changed');


var routes = {
    html: {
      main:   'src/index.html',
      watch:  'src/index.html',
      output: './build'
    },
    styles: {
        main:   './src/css/**/*.css',
        watch:  './src/css/**/*.css',
        output: './build/css'
    },
    scripts: {
        main:   './src/js/**/*.js',
        watch:  './src/js/**/*.js',
        output: './build/js',
    },
    images: {
      main: ['./src/images/**/*.png', './src/images/**/*.jpg'],
      output: './build/images'
    }
};

gulp.task('build:html', function (){
   gulp.src(routes.html.main)
       .pipe(minifyHTML())
       .pipe(gulp.dest(routes.html.output));
});

gulp.task('build:css', function() {
    gulp.src(routes.styles.main)
        // .pipe(uncss({ html: [routes.html.main] }))
        .pipe(minifyCSS())
        .pipe(gulp.dest(routes.styles.output));
});

gulp.task('build:js', function (){
  gulp.src(routes.scripts.main)
    .pipe(uglify())
    .pipe(gulp.dest(routes.scripts.output));
});

gulp.task('build:image', function() {
  gulp.src(routes.images.main)
    .pipe(changeImg(routes.images.output))
    .pipe(imageOP({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
      }))
    .pipe(gulp.dest(routes.images.output));
});


gulp.task('watch', function () {
    gulp.watch(routes.styles.watch, ['build:css']);
    gulp.watch(routes.html.watch,  ['build:html']);
    gulp.watch(routes.scripts.watch,  ['build:js']);
    gulp.watch(routes.images.main,  ['build:image']);
});


gulp.task('build', ['build:css', 'build:html', 'build:js']);
gulp.task('default', ['build', 'watch']);